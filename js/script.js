const menu = document.getElementById('menu');
const map = document.getElementById('map');

// resize header
resizeCache();
window.addEventListener('onload', resizeCache);
window.addEventListener('resize', resizeCache);

// menu and map
if (document.body.classList.contains('content')) {
    menuToggler = document.querySelector('#list .menu-toggler');
    mapToggler = document.querySelector('#list .map-toggler');
}
else {
    menuToggler = document.querySelector('#header-content .menu-toggler');
    mapToggler = document.querySelector('#header-content .map-toggler');
}
// menu toggle
menuToggler.onclick = function(e) {
    menuToggler.setAttribute('aria-expanded', 'true');
    menu.classList.remove('hide');
    document.querySelector('#menu #accessible-button').focus();
    document.addEventListener("keydown", menuController, false);
    // console.log(menuToggler, mapToggler);
}
document.querySelector('#menu .cross').onclick = closeMenu;

function menuController(e) {
    if (e.keyCode === 27) {
        closeMenu();
    }
}
function closeMenu() {
    menuToggler.setAttribute('aria-expanded', 'false');
    menu.classList.add('hide');
    menuToggler.focus();
}

// map toggle
mapToggler.onclick = function(e) {
    map.classList.remove('hide');
    document.querySelector('#map li:first-of-type a').focus();
    map.addEventListener('keydown', mapController, false);
}
document.querySelector('#map .cross').onclick = closeMap;

function mapController(e) {
    if (e.keyCode === 27) {
        closeMap();
    }
    const move = document.querySelectorAll('.move');
    let index = Array.prototype.indexOf.call(move, e.target);
    let x = index % 3;
    let y = Math.floor(index / 3);
    if (e.keyCode === 38) { // up
        index += y == 0 ? 6 : -3;
    }
    if (e.keyCode === 39) { // right
        index += x == 2 ? -2 : 1;
    }
    if (e.keyCode === 40) { // down
        index += y == 2 ? -6 : 3;
    }
    if (e.keyCode === 37) { // left
        index += x == 0 ? 2 : -1;
    }
    move[index].focus();
}
function closeMap() {
    map.classList.add('hide');
    map.removeEventListener('keydown', mapController, false);
}

// page change animation
const nav = document.querySelectorAll('#pages-nav li a');
nav.forEach(function(link) {
    link.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector('body').classList.add('animate');

        if (link.parentElement.classList.contains('up')) {
            document.querySelector('body').classList.add('animateTop');
            const newTopDiv = document.createElement('div');
            newTopDiv.classList.add('newDiv', 'newTopDiv');
            document.querySelector('body').appendChild(newTopDiv);
        }
        if (link.parentElement.classList.contains('right')) {
            document.querySelector('body').classList.add('animateRight');
        }
        if (link.parentElement.classList.contains('bottom')) {
            document.querySelector('body').classList.add('animateBottom');
            const newBottomDiv = document.createElement('div');
            newBottomDiv.classList.add('newDiv', 'newBottomDiv');
            document.querySelector('body').appendChild(newBottomDiv);
        }
        if (link.parentElement.classList.contains('left')) {
            document.querySelector('body').classList.add('animateLeft');
        }

        setTimeout(function () {
            let url = e.target.href;
            window.open(url, '_self');
        }, 100);
    });
});

// enlarge images
if (document.body.classList.contains('content')) {
    const img = document.querySelectorAll('img');
    for(let i = 0; i < img.length; i++) {
        img[i].addEventListener("click", function() {
            window.location = img[i].src;
        });
    }
}

// Archives mode
if (document.querySelector('#archives button')) {

    const archivesButton = document.querySelector('#archives button');

    let today = new Date();
    // let limit = today.setMonth(today.getMonth() - 3);
    let limit = today.setDate(today.getDate() - 1);

    const list = document.querySelectorAll('#list li');


    if (localStorage.getItem('archives-mode') === 'true') {
        toggleArchives(true);
    }
    else {
        list.forEach((li) => {
            hideItems(li, limit);
        });
    }

    archivesButton.onclick = (e) => {
        toggleArchives();
    }

    function toggleArchives(force) {
        const archivesMode = localStorage.getItem('archives-mode') === 'true';
        localStorage.setItem('archives-mode', force || !archivesMode)

        if (force || !archivesMode) {
            document.body.classList.add('archives-mode');
            archivesButton.parentElement.classList.add('extra-space');

            if (window.location.href.includes('fr')) {
                archivesButton.textContent = archivesButton.textContent.replace('Ouvrir', 'Fermer')
            }
            else {
                archivesButton.textContent = archivesButton.textContent.replace('Open', 'Close')
            }
            list.forEach((li) => {
                showItems(li, limit);
            });
        }
        else {
            document.body.classList.remove('archives-mode');
            archivesButton.parentElement.classList.remove('extra-space');
            if (window.location.href.includes('fr')) {
                archivesButton.textContent = archivesButton.textContent.replace('Fermer', 'Ouvrir')
            }
            else {
                archivesButton.textContent = archivesButton.textContent.replace('Close', 'Open')
            }
            list.forEach((li) => {
                hideItems(li, limit);
            });
        }
    }
}

function showItems(item, limit) {
    if (item.querySelector('time')) {
        let date = Date.parse(item.querySelector('time').dateTime);
        if (date < limit) {
            item.classList.remove('hide');
        }
    }
}
function hideItems(item, limit) {
    if (item.querySelector('time')) {
        let date = Date.parse(item.querySelector('time').dateTime);
        if (date < limit) {
            item.classList.add('hide');
        }
    }
}


// Accessible mode
const accessibleButton = document.getElementById('accessible-button');

if (document.body.classList.contains('accessible-class') || localStorage.getItem('accessible-mode') === 'true') {
    switchMode(true);
    if (window.location.href.includes('fr')) {
        accessibleButton.setAttribute('aria-label', 'Désactiver le mode accessible')
    }
    else {
        accessibleButton.setAttribute('aria-label', 'Disable accessible mode')
    }
}
else {
    if (window.location.href.includes('fr')) {
        accessibleButton.setAttribute('aria-label', 'Activer le mode accessible')
    }
    else {
        accessibleButton.setAttribute('aria-label', 'Activate accessible mode')
    }
}

accessibleButton.onclick = () => {
    switchMode();
}

function switchMode(force) {
    const darkmode = localStorage.getItem('accessible-mode') === 'true';
    localStorage.setItem('accessible-mode', force || !darkmode);

    if (force || !darkmode) {
        document.body.classList.add('accessible-mode');
        if (window.location.href.includes('fr')) {
            accessibleButton.setAttribute('aria-label', 'Désactiver le mode accessible')
        }
        else {
            accessibleButton.setAttribute('aria-label', 'Disable accessible mode')
        }
    }
    else {
        document.body.classList.remove('accessible-mode');
        if (window.location.href.includes('fr')) {
            accessibleButton.setAttribute('aria-label', 'Activer le mode accessible')
        }
        else {
            accessibleButton.setAttribute('aria-label', 'Activate accessible mode')
        }
    }
}

// Services and Blog pages specific
if (window.location.href.includes('services/') || window.location.href.includes('blog/')) {
        document.querySelectorAll('body.content #content .text p').forEach((p) => {
        p.classList.add('rm-margin-top');
    });
}
if (window.location.href.includes('services/')) {
    document.querySelectorAll('body.content #content .text h3').forEach((h3) => {
        h3.classList.add('min-margin');
    });
    document.querySelector('#content .text').classList.add('margin-bottom');
}
if (window.location.href.includes('blog/')) {
        document.querySelector('body.content #content').classList.add('margin-top');
}


// tools
function resizeCache() {
    const currentHeight = document.querySelector('h1').offsetHeight;
    document.getElementById('cache').style.cssText = 'height: ' + currentHeight + 'px';
}
